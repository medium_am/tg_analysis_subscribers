import os 
import logging

from telethon import TelegramClient, events, sync
from telethon.tl.types import ChannelAdminLogEventActionParticipantLeave, ChannelAdminLogEventActionParticipantJoin
from services import update_admin_log, get_last_id_from_admin_log


api_id = os.getenv("API_ID")
api_hash = os.getenv("API_HASH")
channel_name = os.getenv("CHANNEL_NAME")
logfile = "server.log"

logging.basicConfig(filename=logfile, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)


client = TelegramClient('session_name', api_id, api_hash)


async def main():
    last_id = get_last_id_from_admin_log()
    logging.debug(f"Last ID from admin log: {last_id}")
    if last_id is None:
        last_id = 0

    admin_log = await client.get_admin_log(channel_name, min_id=last_id, join=True, leave=True)

    update_admin_log(admin_log)


with client:
    client.loop.run_until_complete(main())
