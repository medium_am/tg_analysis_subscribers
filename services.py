import datetime
import logging

from telethon.client import users

from db_utils import check_db_ready, check_table_exists, add_members, create_members_table,\
    update_joined_members, update_leaved_members, create_admin_log_table, get_last_admin_log_id,\
    get_member_by_id, add_admin_log_to_db

from telethon.tl.types import ChannelAdminLogEventActionParticipantLeave, ChannelAdminLogEventActionParticipantJoin


def update_members(members: list) -> None:
    for el in members:
        if el[4] == "join":
            result = get_member_by_id(el[0])
            user_exists = False
            if result is not None:
                user_exists = True

            if user_exists:
                update_joined_members(
                    [[el[0], el[3], True, int(result[6]) + 1]])
            if not user_exists:
                add_members([[el[0], el[1], el[2], el[3], None, True, 1]])

        if el[4] == "leave":
            result = get_member_by_id(el[0])
            user_exists = False
            if result is not None:
                user_exists = True

            if user_exists:
                update_leaved_members([[el[0], el[3], False]])
            if not user_exists:
                add_members([[el[0], el[1], el[2], None, el[3], False, 1]])

    logging.info("Members table was updated")


def update_admin_log(admin_log: list) -> None:
    if check_db_ready():
        if not check_table_exists("admin_log"):
            create_admin_log_table()
        if not check_table_exists("members"):
            create_members_table()

        if check_table_exists("admin_log") and check_table_exists("members"):

            admin_log_for_db = []
            members_from_admin_log = []
            for el in admin_log:
                action = None
                if type(el.action) == ChannelAdminLogEventActionParticipantJoin:
                    action = "join"
                if type(el.action) == ChannelAdminLogEventActionParticipantLeave:
                    action = "leave"
                if action is None:
                    logging.error("Unknown action in event")
                    continue

                admin_log_for_db.append(
                    [el.id, el.user.id, el.user.first_name, el.user.username, el.date, action])
                members_from_admin_log.append(
                    [el.user.id, el.user.first_name, el.user.username, el.date, action])

            members_from_admin_log.sort(key=lambda x: x[3])
            add_admin_log_to_db(admin_log_for_db)
            logging.info("Admin_log table was updated")
            update_members(members_from_admin_log)
        if not check_table_exists("admin_log") or not check_table_exists("members"):
            logging.error("No tables were created")


def get_last_id_from_admin_log() -> int:
    last_id = None
    if check_db_ready():
        if check_table_exists("admin_log"):
            last_id = get_last_admin_log_id()

    return last_id
