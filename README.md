# Telegram channel users analysis
This project collects subscribers to the channel tg and analysis them

## Preferences

1. Install `python`
1. Create env and install requirements
```
python3 -m venv venv
. venv/bin/activate
pip install -U pip
pip install req.txt
```
1. Change *GROUP_ID* in bot_misc.py

### Docker
1. Install `docker`
1. Install `docker-compose`

## Getting started

1. Run DB:
```
docker-compose up -d
```
2. Run application:
```
venv/bin/python server.py
```



