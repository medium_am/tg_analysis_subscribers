import asyncio
import logging

from aiogram import executor, types
from aiogram.types import message
from bot_schedule import scheduler
from bot_misc import bot, dp
from bot_handlers import *


# Configure logging
logfile = "bot.log"
logging.basicConfig(filename=logfile, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)


async def on_startup(x):
    asyncio.create_task(scheduler())


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True, on_startup=on_startup)
