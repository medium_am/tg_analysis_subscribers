import asyncio
import logging
import datetime

from aiogram import executor, types
from aiogram.types import message, ParseMode
from aiogram.types.message import ContentType, ContentTypes
from bot_misc import bot, dp, GROUP_ID
from bot_utils import get_report


@dp.message_handler(commands=['ping'])
async def send_heartbeat(message: types.Message):
    """
    This handler will be called when user sends `/ping` command
    """
    if str(message.chat.id) == str(GROUP_ID):
        await message.reply("pong")


@dp.message_handler(commands=['report'])
async def report(message: types.Message):
    """
    This handler will be called when user sends `/report` command
    """
    if str(message.chat.id) == str(GROUP_ID):
        result = get_report(datetime.datetime.now() - datetime.timedelta(hours=24))
        await message.answer(result, parse_mode=ParseMode.MARKDOWN)
