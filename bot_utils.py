import datetime
from db_utils import check_db_ready, check_table_exists, get_admin_log_by_date,\
    get_count_members
from aiogram.utils.markdown import text, bold, italic, code, pre


def get_report(date: datetime.datetime):
    result = "No data"
    if check_db_ready():
        if check_table_exists("members") and check_table_exists("admin_log"):
            admin_log = get_admin_log_by_date(date.strftime("%Y-%m-%d %H:%M:%S"))
            count_members = get_count_members()

            unique_leaved_member_ids = []
            unique_joined_member_ids = []
            count_leaved = 0
            count_joined = 0

            for el in admin_log:
                user_id = el[1]
                if el[5] == "leave":
                    if user_id in unique_leaved_member_ids:
                        pass
                    if user_id not in unique_leaved_member_ids:
                        unique_leaved_member_ids.append(user_id)
                    
                    if user_id in unique_joined_member_ids:
                        unique_joined_member_ids.remove(user_id)
                    if user_id not in unique_joined_member_ids:
                        pass

                    count_leaved += 1

                if el[5] == "join":
                    if user_id in unique_leaved_member_ids:
                        unique_leaved_member_ids.remove(user_id)
                    if user_id not in unique_leaved_member_ids:
                        pass
                    
                    if user_id in unique_joined_member_ids:
                        pass
                    if user_id not in unique_joined_member_ids:
                        unique_joined_member_ids.append(user_id)

                    count_joined += 1

            result = text("Отчет с ", italic(f"{date.strftime('%b %d %Y %H:%M')}"), " по настоящее время:\n")
            result += text("Всего пользователей в БД - : ", bold(f"{count_members}\n\n"))
            result += text("За данный временной этап:\n")
            result += italic(" Отписались") + text(" - ") + bold(f"{count_leaved}\n")
            result += text("`  Уникальных пдп - `", bold(f"{len(unique_leaved_member_ids)}\n"))
            result += text(italic(" Присоединились")  + text(" - ") + bold(f"{count_joined}\n"))
            result += text("`  Уникальных пдп - `", bold(f"{len(unique_joined_member_ids)}\n"))
    
    return result