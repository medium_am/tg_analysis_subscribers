from aiogram.types.message import ParseMode
import aioschedule as schedule
import asyncio
import datetime

from bot_misc import bot, dp, GROUP_ID
from bot_utils import get_report


async def generate_report_by_channel_users():
    result = get_report(datetime.datetime.now() - datetime.timedelta(hours=24))
    await bot.send_message(GROUP_ID, result, parse_mode=ParseMode.MARKDOWN)


async def scheduler():
    schedule.every().day.at("08:00").do(generate_report_by_channel_users)
    while True:
        await schedule.run_pending()
        await asyncio.sleep(2)