from logging import log
import logging
import psycopg2
import os
from psycopg2.extras import execute_values


con = psycopg2.connect(
    database=os.getenv("PG_DB", "testdb"),
    user=os.getenv("PG_USER", "postgres"),
    password=os.getenv("PG_PASS", "postgres"),
    host=os.getenv("PG_HOST", "127.0.0.1"),
    port=os.getenv("PG_PORT", "5432")
)


def check_db_ready() -> bool:
    if con:
        return True
    else:
        return False


def check_table_exists(table_name: str) -> bool:
    cur = con.cursor()
    cur.execute("select exists(select * from information_schema.tables where table_name=%s)", (table_name,))
    return cur.fetchone()[0]


def create_members_table() -> bool:
    cur = con.cursor()
    try:
        cur.execute('''CREATE TABLE members  
        (user_id INT PRIMARY KEY NOT NULL,
        first_name TEXT,
        username TEXT,
        date_joined TIMESTAMP NULL,
        date_leaved TIMESTAMP NULL,
        status BOOLEAN NOT NULL,
        join_count INT NOT NULL);''')
        con.commit()
        return True

    except psycopg2.Error as e:
        print(e)
        return False


def create_admin_log_table() -> bool:
    cur = con.cursor()
    try:
        cur.execute('''CREATE TABLE admin_log  
        (id BIGINT PRIMARY KEY NOT NULL,
        user_id INT NOT NULL,
        first_name TEXT,
        username TEXT,
        date TIMESTAMP NOT NULL,
        action VARCHAR(30) NOT NULL);''')
        con.commit()
        return True

    except psycopg2.Error as e:
        print(e)
        return False


def get_last_admin_log_id() -> int:
    cur = con.cursor()
    try:
        cur.execute('''
        SELECT MAX(id) FROM admin_log''')
        return cur.fetchone()[0]
    except psycopg2.Error as e:
        logging.error(e)
        return None


def get_member_by_id(id: int) -> list:
    cur = con.cursor()
    cur.execute(f"select * from members where user_id = {id};")
    rows = cur.fetchone()
    return rows


def add_admin_log_to_db(values: list) -> None:
    cur = con.cursor()
    execute_values(cur, "insert into admin_log(id, user_id, first_name, username, date, action) values %s", values)
    con.commit()


def add_members(values: list) -> None:
    cur = con.cursor()
    execute_values(cur, "insert into members(user_id, first_name, username, date_joined, date_leaved, status, join_count) values %s", values)
    con.commit()


def update_joined_members(values: list) -> None:
    cur = con.cursor()
    execute_values(cur, """update members 
    set date_joined = e.date_joined, status = e.status, join_count = e.join_count
    from (values %s) as e(id, date_joined, status, join_count)
    where e.id = members.user_id;""", values)
    con.commit()


def update_leaved_members(values: list) -> None:
    cur = con.cursor()
    execute_values(cur, """update members 
    set date_leaved = e.date_leaved, status = e.status
    from (values %s) as e(id, date_leaved, status)
    where e.id = members.user_id;""", values)
    con.commit()


def get_admin_log_by_date(date: str) -> list:
    cur = con.cursor()
    cur.execute(f"select * from admin_log where date > '{date}' order by date;")
    rows = cur.fetchall()
    return rows


def get_count_members() -> int:
    cur = con.cursor()
    cur.execute("select count(*) from members;")
    return cur.fetchone()[0]